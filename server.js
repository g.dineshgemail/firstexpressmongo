const express = require('express');
const cors = require('cors');
const app = express();
app.use(cors());
const mongoose = require('mongoose');
const port = 3000;
var bodyParser = require('body-parser');
// const { send } = require('process');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const jwt = require('jsonwebtoken');

var dbURL='mongodb://localhost:27017/empdetails';//require('./properties').DB_URL;
mongoose.connect(dbURL);

var db = mongoose.connection;
// db.on('connected', ()=>{
//     console.log('Connected to MongoDB Using Mongoose.');
// });
// db.on('error',  ()=>{
//     console.log('MongoDB connection error:');
// });

var Schema = mongoose.Schema;
var FirstSchema = new Schema({
    age:{
        type:Number,
        validate:[ageValidator, 'Age must be greater than 0.']
    },
  name:String
});
// function that validate the Age
function ageValidator(value) {
    // `this` is the mongoose document
    console.log("Age value is : ", this.age, " : Name value is : ", this.name);
    return value > 0;
}

var personals = mongoose.model('personals', FirstSchema );
//filter
// app.get('/', (req, res) => {
//     console.log("Filters : ", req.query);
//     personals.find(req.query,(err, response)=>{
//         console.log("Records fetched are :  ", response);
//         res.send(response);
//     }).select('name');
//   }).listen(port);

//Filed select & Pagination
// app.get('/', (req, res) => {
//     personals.find((err, results)=>{ 
//         console.log("Pagination Results : ", results);
//         res.send(results);
//     }).select('name').skip(6).limit(2);
//   }).listen(port);

// app.get('/', (req, res) => {
    // personals.find((err, results)=>{ 
    //     console.log("Pagination Results : ", results);
    //     // res.send(results);
    // }).select('-_id').select('-__v').sort({ name: 1 });
//   }).listen(port);

// personals.aggregate([
//     {
//         $group:{
//             _id: '$age',
//             grp:{                
//                 $count:{}
//             },
//             Username:{
//                 $addToSet:'$name'
//             }
//         }
//     },
//     {
//         $project:{
//             Age: '$_id', agecount:1, _id:0, Username:1
//         }
//     }
// ]).then((users) => {
//     console.log("Users : ", users);
// });

// ((err, results)=>{ 
//     console.log("Pagination Results : ", results);
//     // res.send(results);
// }).select('-_id').select('-__v').sort({ name: 1 });

//   app.get('/getvip', (req, res) => {
    // personals.find((err, results)=>{ 
    //     res.send(results);
    // }).select('name').sort({ name: 1 });
//     console.log({name:"Kalam"});
//     res.send( [{name:"Kalam"}] );
//   }).listen(port);

//insert
// personals.create({ name: 'Socrates 4'}, (error)=>{
//     if(error)
//         console.log("Error during insertion : ", error);
//     else
//     	console.log("Record inserted...")
// });

//Validation - insert
// personals.create({ name: 'Socrates 222', age: -1234}, (error)=>{
//     let i = 0;
//     setInterval(function(){
//         console.log("i value : ", i);
//         i=i+1;
//     }, 1000);
//     if(error)
//         console.log("Error during insertion : ", error);
//     else
//     	console.log("Record inserted...")
// });

//Fetching records : 
// var personalData;
// personals.find({ name: 'Socrates'},(err, res)=>{
//     personalData=res;
//     console.log("Records fetched are :  ", personalData);
// });

//Fetching and Sorting records : 
// var personalData;
// personals.find((err, res)=>{
//     personalData=res;
//     console.log("Records fetched are :  ", personalData);
// }).sort({ name: 1 });


// personals.deleteOne( { 'name': 'Socrates 2' },(err, res)=>{
//     console.log("Record Deleted :  ", res);
// });

//update
// let filter = { 'name': 'Socreties 7' };
// let update = { 'name': 'Socreties 14' };
// personals.findOneAndUpdate(filter, update,(err, res)=>{
//     if(err){
//         console.log("Error : ", err);
//     }else
//     console.log("Record Updated :  ")
// });

//update only when Unique
// let filter = { 'name': 'Socreties 8' };
// let update = { 'name': 'Socreties 14' };
// personals.updateOne(filter, update, { upsert: true },(err, res)=>{
//     if(err){
//         console.log("Error : ", err);
//     }else
//     console.log("Record Updated :  ")
// });

//Filtering
// app.get('/', (req, res, next) => {
//     const filters = req.query;
//     console.log("Filters : ", filters);
//     res.send(filters);
// }).listen(3000);

  //Select Fileds
//   personals.find({ age: 2020}, (err, results)=>{ 
//       console.log("Pagination Results : ", results);
//    }).select('name');

  //Pagination
//   personals.find({ age: 2020}, (err, results)=>{ 
//     console.log("Pagination Results : ", results);
//  }).select('name').skip(0).limit(2);

//Fetch From UI
// app.get('/getvip',function(req, sendres){
//     personals.find((err, res)=>{
//         console.log("Records fetched are :  ", res);
//         sendres.send(res);
//     }).select('name');
//     // sendres.send({course:"MEAN Stack"});
// }).listen(3000);

//Insert from UI
// app.post('/postUsers',function(req, sendres){
//     // const body = req.body;
//     const body = req.params;
//     console.log(" : Post Data from Angular : ", body);
//     // let body = {name:'Socrates 5', age: 2500};
//     // personals.create(body, (error)=>{
//     //     if(error)
//     //         console.log("Error during insertion : ", error);
//     //     else
//     //         console.log("Record inserted...")
//     // });
//     sendres.send( body);
// }).listen(3000);

// app.get('/', (req, res, next) => {
//     console.log("Filters : ", req.query);
//     res.send(req.query);
//   }).listen(3000);

//Delete from UI
// app.post('/postUsers',function(req, sendres){
//     const body = req.body;
//     console.log(body.name, " : Delete Data from Angular : ", body);
//     personals.deleteOne( body,(err, res)=>{
//         console.log("Record Deleted :  ", res);
//     });
//     sendres.send("Delete Successfully Completed");
// }).listen(3000);

//update
// app.post('/postUsers',function(req, sendres){
//     const body = req.body;
//     console.log(body.name, " : Delete Data from Angular : ", body);

//     let filter = { 'name': 'Socrates 4' };
//     let update = body;
//     personals.updateOne(filter, update, { upsert: true },(err, res)=>{
//         if(err){
//             console.log("Error : ", err);
//         }else
//         console.log("Record Updated :  ")
//     });

//     sendres.send("Update Successfully Completed");
// }).listen(3000);

// app.get('/red', (req, res, next) => {
//     console.log("Filters : ", req.query);
//     res.send(req.query);
//   });

// app.get('*',(req, res, next) => {
//     console.log("Filters : ", req.query);
//     res.send({message : "You are not authorised to this Page. :("});
//   });
  
  app.listen(3000, () => console.log("Running on port 3000."));

  app.get('/login', (req, res, next) => {
    const data = { userType : 11, dept : 801, role:207 };
    const token = jwt.sign({data}, 'my_secret_key');
    res.send({
        message:'Authentication Successfull',
        token
    });
  });

  app.get('/protectedHome', checkAuth, (req, res, next) => {
    console.log("protectedHome : ");
    res.send({
        message:'You\'re Authorized User.'
    });
  });

  function checkAuth(req, res, next){
      let token = req.headers['authorization'];
      let bearerHeader = token.split(" ")[1];
      console.log("Token : ", token, " : bearerHeader : ",bearerHeader);
      jwt.verify(bearerHeader, 'my_secret_key', function(err, data){
        if(err){
            res.sendStatus(403);
        }else{
            res.json({
                text:'This is Protected',
                data
            });
            // next();
        }
      })
    //   let token = false;
    //   if(token){
    //       next();
    //   }else{
    //     return res.status(401).json({
    //         message:"Auth failed"
    //     });
    //   }
}
