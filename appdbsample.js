const express = require('express');
var app=express();
const cors = require('cors');
app.use(cors());
const mongoose=require('mongoose');
var dbURL=require('./properties').DB_URL;
mongoose.connect(dbURL);
// mongoose.connect("mongodb://localhost/empdetails");
const port = 3000;

var db = mongoose.connection;
db.on('connected', ()=>{
    console.log('Connected to MongoDB Using Mongoose.')
});
db.on('error',  ()=>{
    console.log('MongoDB connection error:');
});

var Schema = mongoose.Schema;
var FirstSchema = new Schema({
  age: Number,
  name: String
});

// Compile model from schema
var personals = mongoose.model('personals', FirstSchema );

// personals.create({ name: 'Socreties 8',age:2000 }, (error)=>{
//     if(error)
//         console.log("Error during insertion : ", error);
//     else
//     console.log("Record inserted...")
// });

//fetch all
// personals.find((err, res)=>{
// console.log("All Records fetched :  ", res)
// });

// fetch with condition
// personals.find( { 'name': 'Socreties 3' },(err, res)=>{
//     console.log("Record fetched is :  ", res)
// });

// Delete record
// personals.deleteOne( { 'name': 'Socreties 3' },(err, res)=>{
//     console.log("Record Deleted :  ", res)
// });

// personals.find((err, res)=>{
// console.log("Records fetched are :  ", res)
// });

let filter = { 'name': 'Socreties 4' };
let update = { 'name': 'Socreties 14' }
personals.findOneAndUpdate(filter, update,(err, res)=>{
    console.log("Record Updated :  ")
});