// var http = require("http");// not used

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const port = 3000;

const app = express();
app.use(cors());

//body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/',function(req, res){
    //res.send("Foobar");
    res.send({ name: "Alten" });
});

app.get('/getUsers',function(req, res){
    //res.send("Foobar");
    res.send({ name: "Alexander" });
});

/*
http.createServer(function(req, res){
    res.end("Hello from node JS");
}).listen(3000);
*/

app.listen(port, ()=>{
    console.log("Server is running on the port : ", port);
});
console.log("Node is working");
